package com.relayer.playground.jee.service.contract;

import javax.ejb.Local;

@Local
public interface ShoppingCartServiceLocal {
    void listContents();
    void cancelShoppingCart();
    void applyRebate(int rate);
}
