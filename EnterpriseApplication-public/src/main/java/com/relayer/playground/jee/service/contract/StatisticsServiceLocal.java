package com.relayer.playground.jee.service.contract;

import javax.ejb.Local;

@Local
public interface StatisticsServiceLocal {
    long getHitCounter();
    void incrementHitCounter();
}
