package com.relayer.playground.jee.managed;

import com.relayer.playground.jee.service.contract.EditorServiceLocal;
import com.relayer.playground.jee.service.contract.ShoppingCartServiceLocal;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class MediatorBean {
    @EJB
    private ShoppingCartServiceLocal shoppingCartService;

    @EJB
    private EditorServiceLocal editorService;

    public String doNothing() {
        editorService.offerRebate();
        shoppingCartService.listContents();
        return "status.xhtml";
    }
}
