package com.relayer.playground.jee.service.singleton;

import com.relayer.playground.jee.service.contract.StatisticsServiceLocal;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Startup;

@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Singleton
@Startup
@DependsOn({"BootstrapService"})
public class StatisticsService implements StatisticsServiceLocal {
    private long hitCounter;

    protected StatisticsService() {
        hitCounter = 0L;

        System.out.println("Hello World from StatisticsService");
        System.out.flush();
    }

    @Lock(LockType.READ)
    @Override
    public long getHitCounter() {
        return hitCounter;
    }

    @Lock(LockType.WRITE)
    @Override
    public void incrementHitCounter() {
        hitCounter++;
    }
}
