package com.relayer.playground.jee.service.singleton;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;

@Singleton
@LocalBean
public class FailingSingletonServiceWithStartup {
    // Raising an exception in a @PostConstruct method without @Startup => application is deployed and the faulty bean discarded
    @PostConstruct
    private void failingMethod() {
        System.out.println("I am going to fail but the application is going to be deployed");
        System.out.println("I am also going to be discarded");
        System.out.flush();
//        System.out.println(1/0);
    }

    public String doNothing() {
        System.out.println("I do nothing");
        System.out.flush();
        return "index.xhtml";
    }
}
