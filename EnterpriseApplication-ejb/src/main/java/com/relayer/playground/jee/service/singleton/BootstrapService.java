package com.relayer.playground.jee.service.singleton;

import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class BootstrapService {
    protected BootstrapService() {
        System.out.println("Hello World from BootstrapService");
    }
}
