package com.relayer.playground.jee.service;

import com.relayer.playground.jee.service.contract.ShoppingCartServiceLocal;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.ejb.Stateful;

@Stateful
public class ShoppingCartService implements ShoppingCartServiceLocal {
    private int rate;

    protected ShoppingCartService() {
        rate = 0;
    }

    @PostConstruct
    @PostActivate
    public void init() {
    }

    @PreDestroy
    @PrePassivate
    public void cleanup() {        
    }

    @Remove
    @Override
    public void cancelShoppingCart() {
    }

    @Override
    public void listContents() {
        System.out.println("List shopping cart contents");
        System.out.flush();
    }

    @Override
    public void applyRebate(final int rate) {
        this.rate = rate;

        System.out.println("Applying rate " + rate + "%");
        System.out.flush();
    }
}
