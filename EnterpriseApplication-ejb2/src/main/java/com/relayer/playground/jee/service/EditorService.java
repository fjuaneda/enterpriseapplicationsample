package com.relayer.playground.jee.service;

import com.relayer.playground.jee.service.contract.EditorServiceLocal;
import com.relayer.playground.jee.service.contract.ShoppingCartServiceLocal;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;

@Stateless
@Local({EditorServiceLocal.class})
public class EditorService implements EditorServiceLocal {
    @EJB
    private ShoppingCartServiceLocal shoppingCartService;

    @PostConstruct
    private void setup() {
        System.out.println("Shopping cart injected");
        System.out.flush();
    }

    @Override
    public void offerRebate() {
        shoppingCartService.applyRebate(5);

        System.out.println("Offer rebate of 5%");
        System.out.flush();
    }
}
