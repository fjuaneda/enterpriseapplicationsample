package com.relayer.playground.jee.service;

import com.relayer.playground.jee.service.contract.StatisticsServiceLocal;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.PostActivate;

@Stateless
@LocalBean
public class BookService {
    @EJB
    private StatisticsServiceLocal statisticsService;

    @PostConstruct
    @PostActivate
    public void init() {
        statisticsService.getHitCounter();
    }
}
